<div class="container">

    <div class="mt-4">
        <a class="btn btn-primary" data-mdb-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            Nova Conta
        </a>
    </div>            
    
    <div class="collapse mt-3" id="collapseExample">
        <div class="row">
            <div class="col-md-6 mx-auto border mt-5 pt-3 pb-3">
                <form method="POST" id="contas-form">
                
                    <input class="form-control" name="parceiro" type="text" placeholder="Devedor / Credor"><br>
                    <input class="form-control" name="descricao" type="text" placeholder="Descrição"><br>
                    <input class="form-control" name="valor" type="number" placeholder="Valor"><br>

                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" min="0" name="number" class="form-control" placeholder="Mês">
                        </div>
                        <div class="col-md-6">
                            <input type="number" min="0" name="ano" class="form-control" placeholder="Ano">
                        </div>
                    </div>
                    <input type="hidden" name="tipo" value="<?= $tipo ?>"><br>                

                    <div class="text-center text-md-left">
                        <a class="btn btn-primary" onclick="document.getElementById('contas-form').submit();">Enviar</a>
                    </div>
                </form>    
            </div>
        </div>
    </div>
</div>

<div class="row mt-5">
    <div class="col">
        <?= $lista ?>
    </div>
</div>