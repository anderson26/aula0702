<div style="height: 100vh">
    <div class="flex-center flex-column">
        <h3 class="mb-5">Controle Financeiro Pessoal</h3>
        <form class="border boder-light p-5" method="POST">
            <div class="form-outline mb-4">
                <input type="email" id="email" name="email" class="form-control" />
                <label class="form-label">E-mail</label>
            </div>

            <div class="form-outline mb-4">
                <input type="password" id="senha" name="senha" class="form-control" />
                <label class="form-label">Senha</label>
            </div>

            <!-- <div class="row mb-4">
                <div class="col d-flex justify-content-center">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="form1Example3" checked />
                        <label class="form-check-label" for="form1Example3"> Lembrar-me </label>
                    </div>
                </div>

                <div class="col">
                    <a href="#!">Esqueceu a senha?</a>
                </div>
            </div> -->

            <button type="submit" class="btn btn-primary btn-block">Entrar</button>
            <p class="red-text pt-3"><?= $error ? 'Dados de acesso incorretos!' : ' ' ?></p>
        </form>
    </div>
</div>